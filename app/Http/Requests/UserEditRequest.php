<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('usuario');
        return [
            'name' => 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'apellidoPa'=> 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'apellidoMa'=> 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'email'=> 'required|max:50|email|unique:users,email,' . $user,
            'password'=> 'nullable|min:8|confirmed',
        ];
    }
}
