<?php

namespace App\Http\Requests\Mascotas;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Nombre'=> 'required|max: 30|regex:/^[\pL\s\-]+$/u',
            'animal'=> 'required|max: 30|regex:/^[\pL\s\-]+$/u',
            'tiempo'=> 'required|in:Meses,Años',
            'edad'=> 'required|not_in:0|digits_between:1,100',
            'img'=> 'required',
            'img.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'DescripMedica'=> 'required|regex:/^[\pL\s\-0-9.,:;¿?!¡°]+$/u',
            'tel'=> 'digits:10'
        ];
    }
}
