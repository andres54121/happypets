<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:10|unique:roles|regex:/^[\pL\s\-]+$/u',
            'description'=> 'required|max:50|regex:/^[\pL\s\-0-9.,]+$/u',
        ];
    }
}
