<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->rol == 'Admin'){
            return view('dashboard');
        }
        else{
            return redirect()->route("home");
        }
    }

    public function indexUser()
    {
        if(Auth::user()->rol == 'Admin'){

            $users = User::all();
            return view('admin.pedidos', compact('users'));
        }
        else{
            return redirect()->route("home");
        }
    }
}
