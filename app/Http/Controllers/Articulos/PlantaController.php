<?php

namespace App\Http\Controllers\Articulos;

use Illuminate\Http\Request;
use App\Models\Articulos\Planta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Articulos\imgPlantas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Articulos\PlantaEditRequest;
use App\Http\Requests\Articulos\PlantaCreateRequest;

class PlantaController extends Controller
{
    const PERMISSIONS = [
        'create' => 'planta.create',
        'edit' => 'planta.edit',
        'destroy' => 'planta.destroy',
        // '' => '',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['create'])->only(['create', 'store']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['edit'])->only(['edit', 'update', 'eliminarimagen']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['destroy'])->only(['destroy']);
    }

    public function index()
    {
        if(Auth::user()->rol == 'usuario'){
            return redirect()->route("home");
        }
        $plantas = Planta::paginate(10);
        return view("admin.Articulos.Planta.index", compact('plantas'));
    }

    public function create()
    {
        return view('admin.Articulos.Planta.create');
    }

    public function store(PlantaCreateRequest $request)
    {
        $planta = new Planta();
        $planta->Nombre = $request->Nombre;
        $planta->Tipo = $request->Tipo;
        $planta->Cantidad = $request->Cantidad;
        $planta->Precio = $request->Precio;
        $planta->Descripcion = $request->Descripcion;
        if(!empty($request->Cuidados))
            $planta->Cuidados = $request->Cuidados;
        $planta->Imagenes = date('dmyHi');
        $planta->save();

        $image = $request->file('img');
        if(!empty($image)){

            foreach($request->file('img') as $image)
            {
                $imagen = uniqid() . $image->getClientOriginalName();
                $formato = $image->getClientOriginalExtension();
                $image->move(public_path().'/uploads/planta/', $imagen);

                // Guardamos el nombre de la imagen en la tabla 'img_bicicletas'
                DB::table('img_plantas')->insert(
                    [
                        'nombre' => $imagen,
                        'formato' => $formato,
                        'planta_id' => $planta->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]
                );
            }
        }

        return redirect()->route("plantas.index")->with('success', 'Nueva planta agregada correctamente');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $planta = Planta::findOrFail($id);
        $imagenes = imgPlantas::where('planta_id', '=', $id)->get();
        return view('admin.Articulos.Planta.edit', compact('imagenes'), ['planta'=> $planta]);
    }

    public function update(PlantaEditRequest $request, $id)
    {
        $planta = Planta::findOrFail($id);
        $planta->update($request->all());
        // Actualizar imagenes
        $ci = $request->file('img');

        // Si la variable '$ci' no esta vacia, actualizamos el registro con las nuevas imágenes
        if(!empty($ci)){

            // Recibimos una o varias imágenes y las actualizamos
            foreach($request->file('img') as $image)
                {
                    $imagen = uniqid() . $image->getClientOriginalName();
                    $formato = $image->getClientOriginalExtension();
                    $image->move(public_path().'/uploads/planta/', $imagen);

                    // Actualizamos el nuevo nombre de la(s) imagen(es) en la tabla 'img_bicicletas'
                    DB::table('img_plantas')->insert(
                        [
                            'nombre' => $imagen,
                            'formato' => $formato,
                            'planta_id' => $planta->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        ]
                    );
                }
        }
        return redirect()->route("plantas.index")->with('success', 'Planta actualizada correctamente');
    }

    public function destroy($id)
    {
        $planta = Planta::findOrFail($id);

        // Selecciono las imágenes a eliminar
        $imagen = DB::table('img_plantas')->where('planta_id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');

        // Creamos una lista con los nombres de las imágenes separadas por coma
        $imagenes = explode(",", $imgfrm);

        // Recorremos la lista de imágenes separadas por coma
        foreach($imagenes as $image){

            // Elimino la(s) imagen(es) de la carpeta 'uploads'
            $dirimgs = public_path().'/uploads/planta/'.$image;

            // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
            if(File::exists($dirimgs)) {
                File::delete($dirimgs);
            }

        }
        $planta->imagenesPlantas()->delete();
        $planta->delete();
        return redirect()->route("plantas.index")->with('success', 'Eliminado correctamente');

    }

    // Eliminar imagen de un Registro
    public function eliminarimagen($id)
    {
        // Elimino la imagen de la carpeta 'uploads'
        $imagen = imgPlantas::select('nombre')->where('id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');

        $planta = imgPlantas::select('planta_id')->where('id', '=', $id)->get();
        $idP = $planta->implode('planta_id', ',');
        // Elimino la(s) imagen(es) de la carpeta 'uploads'
        $dirimgs = public_path().'/uploads/planta/'.$imgfrm;

        // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
        if(File::exists($dirimgs)) {
            File::delete($dirimgs);
        }
        imgPlantas::destroy($id);

        return Redirect::to('administracion/plantas/'.$idP.'/edit');

    }

    // Cambiar el estado de la planta
    public function changeStatus($id)
    {
        $planta = Planta::findOrFail($id);
        if($planta->Estado == 'Habilitado'){
            $planta->Estado = 'Deshabilitado';
            $planta->save();
        }
        else if($planta->Estado == 'Deshabilitado'){
            $planta->Estado = 'Habilitado';
            $planta->save();
        }

        return redirect()->route("plantas.index")->with('success', 'Estado de la planta actualizada correctamente');
    }
}
