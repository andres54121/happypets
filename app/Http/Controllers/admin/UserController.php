<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserCreateRequest;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:SuperAdmin']);
    }

    public function index()
    {
        $users = User::whereDoesntHave('roles', function ($query) {
            $query->where('name', 'user');
        })->paginate(10);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    public function store(UserCreateRequest $request)
    {
        User::create($request->only('name', 'apellidoPa', 'apellidoMa', 'email', 'rol')
            + [
                'password' => bcrypt($request->input('password'))
            ])->roles()->sync($request->role);
            // ->assignRole('admin')
        return redirect()->route("usuario.index")->with('success', 'Usuario creado correctamente');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(UserEditRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->only('name', 'email', 'apellidoPa', 'apellidoMa', 'rol');

        if(trim($request->password)== ''){
            $data = $request->except('password');
        }
        else{
            $data = $request->all();
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);
        $user->roles()->sync($request->role);
        return redirect()->route("usuario.index")->with('success', 'Usuario actualizado correctamente');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route("usuario.index")->with('success', 'Usuario eliminado correctamente');
    }

}
