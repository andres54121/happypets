<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:SuperAdmin'])->only(['index']);
    }

    public function index()
    {
        $permissions = Permission::orderBy('name')->paginate(10);
        return view('admin.permission.index', compact('permissions'));
    }
}
