<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mascota;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\imgMascotas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Mascotas\EditRequest;
use App\Http\Requests\Mascotas\CreateRequest;

class MascotaController extends Controller
{
    const PERMISSIONS = [
        'create' => 'mascota.create',
        'edit' => 'mascota.edit',
        'destroy' => 'mascota.destroy',
        // '' => '',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['create'])->only(['create', 'store']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['edit'])->only(['edit', 'update', 'eliminarimagen']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['destroy'])->only(['destroy']);
    }

    public function index()
    {
        if(Auth::user()->rol == 'usuario'){
            return redirect()->route("home");
        }
        $mascotas = Mascota::paginate(10);
        return view("admin.mascotas.index", compact('mascotas'));
    }

    public function create()
    {
        return view('admin.mascotas.create');
    }

    public function store(CreateRequest $request)
    {
        $mascota = new Mascota();
        $mascota->Nombre = $request->Nombre;
        $mascota->edad = $request->edad;
        $mascota->tiempo = $request->tiempo;
        $mascota->animal = $request->animal;
        $mascota->tel = $request->tel;
        $mascota->DescripMedica = $request->DescripMedica;
        $mascota->Imagenes = date('dmyHi');
        echo $mascota;
        $mascota->save();

        $image = $request->file('img');
        if(!empty($image)){

            foreach($request->file('img') as $image)
            {
                $imagen = uniqid() . $image->getClientOriginalName();
                $formato = $image->getClientOriginalExtension();
                $image->move(public_path().'/uploads/mascotas/', $imagen);

                // Guardamos el nombre de la imagen en la tabla 'img_bicicletas'
                DB::table('img_mascota')->insert(
                    [
                        'nombre' => $imagen,
                        'formato' => $formato,
                        'mascota_id' => $mascota->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]
                );
            }
        }

        return redirect()->route("mascotas.index")->with('success', 'Nueva mascota agregada correctamente');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $mascota = Mascota::findOrFail($id);
        $imagenes = imgMascotas::where('mascota_id', '=', $id)->get();
        return view('admin.mascotas.edit', compact('imagenes'), ['mascota'=> $mascota]);
    }

    public function update(EditRequest $request, $id)
    {
        $mascota = Mascota::findOrFail($id);
        $mascota->update($request->all());
        // Actualizar imagenes
        $ci = $request->file('img');

        // Si la variable '$ci' no esta vacia, actualizamos el registro con las nuevas imágenes
        if(!empty($ci)){

            // Recibimos una o varias imágenes y las actualizamos
            foreach($request->file('img') as $image)
                {
                    $imagen = uniqid() . $image->getClientOriginalName();
                    $formato = $image->getClientOriginalExtension();
                    $image->move(public_path().'/uploads/mascotas/', $imagen);

                    // Actualizamos el nuevo nombre de la(s) imagen(es) en la tabla 'img_bicicletas'
                    DB::table('img_mascota')->insert(
                        [
                            'nombre' => $imagen,
                            'formato' => $formato,
                            'mascota_id' => $mascota->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        ]
                    );
                }
        }
        return redirect()->route("mascotas.index")->with('success', 'Informacion de la mascota actualizada correctamente');
    }

    public function destroy($id)
    {
        $mascota = Mascota::findOrFail($id);

        // Selecciono las imágenes a eliminar
        $imagen = DB::table('img_mascota')->where('mascota_id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');

        // Creamos una lista con los nombres de las imágenes separadas por coma
        $imagenes = explode(",", $imgfrm);

        // Recorremos la lista de imágenes separadas por coma
        foreach($imagenes as $image){

            // Elimino la(s) imagen(es) de la carpeta 'uploads'
            $dirimgs = public_path().'/uploads/mascotas/'.$image;

            // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
            if(File::exists($dirimgs)) {
                File::delete($dirimgs);
            }

        }
        $mascota->imagenesMascotas()->delete();
        $mascota->delete();
        return redirect()->route("mascotas.index")->with('success', 'Eliminado correctamente');

    }

    // Eliminar imagen de un Registro
    public function eliminarimagen($id)
    {
        // Elimino la imagen de la carpeta 'uploads'
        $imagen = imgMascotas::select('nombre')->where('id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');

        $mascota = imgMascotas::select('mascota_id')->where('id', '=', $id)->get();
        $idP = $mascota->implode('mascota_id', ',');
        // Elimino la(s) imagen(es) de la carpeta 'uploads'
        $dirimgs = public_path().'/uploads/mascotas/'.$imgfrm;

        // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
        if(File::exists($dirimgs)) {
            File::delete($dirimgs);
        }
        imgMascotas::destroy($id);

        return Redirect::to('administracion/mascotas/'.$idP.'/edit');

    }
}
