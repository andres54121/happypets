<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
    use HasFactory;

    protected $table = 'mascotas';

    protected $fillable  = [
        "Id",
        "Nombre",
        "edad",
        "tiempo",
        "animal",
        "tel",
        "Imagenes",
        "DescripMedica",
    ];

    // Relación One to Many (Uno a muchos), un registro puede tener muchas imágenes
    public function imagenesMascotas()
    {
        return $this->hasMany('App\Models\imgMascotas');
    }
}

