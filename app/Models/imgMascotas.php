<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imgMascotas extends Model
{
    use HasFactory;
    protected $table = 'img_mascota';

    protected $fillable = [
        'nombre',
        'formato',
        'mascota_id',
    ];

    public function mascota()
    {
    	return $this->belongsTo('App\Models\Mascota');
    }
}
