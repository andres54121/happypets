@extends('layouts.Shoppingcart')

@section('title','Bienvenido a HappyPets')
<!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'landing-page sidebar-collapse')
<!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
<style>
    .row {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }

    .row>[class*='col-'] {
        display: flex;
        flex-direction: column;
    }

</style>
@endsection

@section('content')

<!--jumbotron-->
<div class="page-header header-filter" data-parallax="true" style="background-image: url({{ asset('img/animalitos/dog-4231250_1920.jpg') }})">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('img/animalitos/banner1.png') }}" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('img/animalitos/banner 3.png') }}" class="d-block w-100" alt="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end jumbotron-->


<!--principal content, similar al landing page-->
<div class="main main-raised">
    <!-- Products section -->
    <div class="section text-center">
        <h2 class="title">Mascotas</h2>
        <br>
    </div>

    <div class="container mb-5">

        <div class="section section-contacts  ">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="text-center title">Trabajamos para ti</h2>
                    <h4 class="text-center description">Si aún no te registras, puedes enviarnos tus dudas y te responderemos a la brevedad.</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">¿Cuál es tu nombre?</label>
                                <input type="text" name="name_user" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Tu Correo Electronico</label>
                                <input type="email" name="email" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleMessage" class="bmd-label-floating">Plantea tus dudas</label>
                        <textarea type="text" name="message" class="form-control" rows="4" id="exampleMessage"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-4 ml-auto mr-auto text-center">
                            <button class="btn btn-primary btn-raised">
                                Enviar Consulta
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>
<!--end principal content-->
@endsection

@section('scripts')
<script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
<script>
    $(function() {
        // Inicializamos el motor de busqueda que realmente hace las sugerencias
        // bundle: una agrupacion de ambos
        var products = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace
            , queryTokenizer: Bloodhound.tokenizers.whitespace
            , prefetch: '{{ url("/products/json") }}'
        });

        // Inicializamos typeahead sobre el input de busqueda
        $('#search').typeahead({
            //propiedades
            hint: true
            , highlight: true
            , minLength: 1
        }, {
            // dataset
            name: 'products'
            , source: products
        });

    });

</script>
@endsection
