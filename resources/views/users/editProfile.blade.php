@extends('layouts.Shoppingcart')

@section('title', 'Perfil de Usuario')
    <!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'profile-page sidebar-collapse')
    <!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
@endsection

@section('content')

    <!--jumbotron-->
    <div class="page-header header-filter" data-parallax="true"
        style="background-image: url({{ asset('img/cover.jpg') }})">
    </div>
    <!-- end jumbotron-->

    <div class="main main-raised">
        <div class="container">
            <br>
            <form action="{{ route('profile') }}" method="post" class="form-horizontal">
                @csrf
                <div class="text-center ">
                    <h3 for="name" class="title">Nombre Completo</h3>
                </div>
                <div class="container text-center row">
                    <div class="form-group col-md-6">
                        <label for="SMza"><b>Nombre(s)</b></label>
                        <input type="text" name="name" class="form-control" value="{{ old('name', Auth::user()->name) }}"
                            placeholder="Nombre" autofocus required>
                        @if ($errors->has('name'))
                            <span class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="SMza"><b>Apellido Paterno</b></label>
                        <input type="text" name="apellidoPa" class="form-control"
                            value="{{ old('apellidoPa', Auth::user()->apellidoPa) }}" placeholder="Apellido Paterno"
                            required>
                        @if ($errors->has('apellidoPa'))
                            <span class="error text-danger"
                                for="input-apellidoPa">{{ $errors->first('apellidoPa') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="SMza"><b>Apellido Materno</b></label>
                        <input type="text" name="apellidoMa" class="form-control"
                            value="{{ old('apellidoMa', Auth::user()->apellidoMa) }}" placeholder="Apellido Materno"
                            required>
                        @if ($errors->has('apellidoMa'))
                            <span class="error text-danger"
                                for="input-apellidoMa">{{ $errors->first('apellidoMa') }}</span>
                        @endif
                    </div>
                </div>

                {{-- Numero de Telefono --}}
                <div class="text-center ">
                    <h3 for="name" class="title">Telefono</h3>
                </div>
                <div class="container text-center"><input type="tel" name="phone" class="form-control"
                        value="{{ old('phone', Auth::user()->phone) }}" placeholder="Numero de Telefono" required>
                    @if ($errors->has('phone'))
                        <span class="error text-danger" for="input-phone">{{ $errors->first('phone') }}</span>
                    @endif
                </div>
                {{-- Direccion --}}
                <div class="text-center ">
                    <h3 for="name" class="title">Direccion</h3>
                </div>
                @php $address = App\Models\Address::find(Auth::user()->id_direccion); @endphp
                @if (empty($address))
                <div class="container text-center row">
                    <div class="form-group col-md-5">
                        <label for="SMza"><b>Calle Principal</b></label>
                        <input type="text" name="CalleP" class="form-control" value="{{ old('CalleP') }}"
                            placeholder="Calle Principal" required>
                        @if ($errors->has('CalleP'))
                            <span class="error text-danger" for="input-CalleP">{{ $errors->first('CalleP') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-7">
                        <label for="SMza"><b>Calles</b></label>
                        <input type="text" name="entreCalles" class="form-control" value="{{ old('entreCalles') }}"
                            placeholder="Entre calles..." required>
                        @if ($errors->has('entreCalles'))
                            <span class="error text-danger"
                                for="input-entreCalles">{{ $errors->first('entreCalles') }}</span>
                        @endif
                    </div>
                </div>
                <div class="container text-center row">
                    <div class="form-group col-md-2">
                        <label for="SMza"><b>SMza</b></label>
                        <input type="number" name="SMza" class="form-control" placeholder="SMza" required
                            value="{{ old('SMza') }}">
                        @if ($errors->has('SMza'))
                            <span class="error text-danger" for="input-SMza">{{ $errors->first('SMza') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="Mza"><b>Mza</b></label>
                        <input type="number" name="Mza" class="form-control" placeholder="Mza" required
                            value="{{ old('Mza') }}">
                        @if ($errors->has('Mza'))
                            <span class="error text-danger" for="input-Mza">{{ $errors->first('Mza') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="Lt"><b>Lt</b></label>
                        <input type="number" name="Lt" class="form-control" placeholder="Lt" required
                            value="{{ old('Lt') }}">
                        @if ($errors->has('Lt'))
                            <span class="error text-danger" for="input-Lt">{{ $errors->first('Lt') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Colonia"><b>Colonia</b></label>
                        <input type="text" name="Colonia" class="form-control" placeholder="Colonia" required
                            value="{{ old('Colonia') }}">
                        @if ($errors->has('Colonia'))
                            <span class="error text-danger" for="input-Colonia">{{ $errors->first('Colonia') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="CP"><b>CP</b></label>
                        <input type="number" name="CP" class="form-control" placeholder="CP" required
                            value="{{ old('CP') }}">
                        @if ($errors->has('CP'))
                            <span class="error text-danger" for="input-CP">{{ $errors->first('CP') }}</span>
                        @endif
                    </div>
                </div>

                <div class="container text-center">
                    <div class="form-group ">
                        <label for="Descripcion"><b>Descripcion</b></label>
                        <textarea class="form-control" placeholder="Por una casa de color..." name="Descripcion">{{ old('Descripcion') }}</textarea>
                        @if ($errors->has('Descripcion'))
                            <span class="error text-danger"
                                for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
                        @endif
                    </div>
                </div>
                @else
                <div class="container text-center row">
                    <div class="form-group col-md-5">
                        <label for="SMza"><b>Calle Principal</b></label>
                        <input type="text" name="CalleP" class="form-control" value="{{ old('CalleP', $address->CalleP) }}"
                            placeholder="Calle Principal" required>
                        @if ($errors->has('CalleP'))
                            <span class="error text-danger" for="input-CalleP">{{ $errors->first('CalleP') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-7">
                        <label for="SMza"><b>Calles</b></label>
                        <input type="text" name="entreCalles" class="form-control" value="{{ old('entreCalles', $address->entreCalles) }}"
                            placeholder="Entre calles..." required>
                        @if ($errors->has('entreCalles'))
                            <span class="error text-danger"
                                for="input-entreCalles">{{ $errors->first('entreCalles') }}</span>
                        @endif
                    </div>
                </div>
                <div class="container text-center row">
                    <div class="form-group col-md-2">
                        <label for="SMza"><b>SMza</b></label>
                        <input type="number" name="SMza" class="form-control" placeholder="SMza" required
                            value="{{ old('SMza', $address->SMza) }}">
                        @if ($errors->has('SMza'))
                            <span class="error text-danger" for="input-SMza">{{ $errors->first('SMza') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="Mza"><b>Mza</b></label>
                        <input type="number" name="Mza" class="form-control" placeholder="Mza" required
                            value="{{ old('Mza', $address->Mza) }}">
                        @if ($errors->has('Mza'))
                            <span class="error text-danger" for="input-Mza">{{ $errors->first('Mza') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="Lt"><b>Lt</b></label>
                        <input type="number" name="Lt" class="form-control" placeholder="Lt" required
                            value="{{ old('Lt', $address->Lt) }}">
                        @if ($errors->has('Lt'))
                            <span class="error text-danger" for="input-Lt">{{ $errors->first('Lt') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Colonia"><b>Colonia</b></label>
                        <input type="text" name="Colonia" class="form-control" placeholder="Colonia" required
                            value="{{ old('Colonia', $address->Colonia) }}">
                        @if ($errors->has('Colonia'))
                            <span class="error text-danger" for="input-Colonia">{{ $errors->first('Colonia') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="CP"><b>CP</b></label>
                        <input type="number" name="CP" class="form-control" placeholder="CP" required
                            value="{{ old('CP', $address->CP) }}">
                        @if ($errors->has('CP'))
                            <span class="error text-danger" for="input-CP">{{ $errors->first('CP') }}</span>
                        @endif
                    </div>
                </div>

                <div class="container text-center">
                    <div class="form-group ">
                        <label for="Descripcion"><b>Descripcion</b></label>
                        <textarea class="form-control" placeholder="Por una casa de color..." name="Descripcion">{{ old('Descripcion', $address->Descripcion) }}</textarea>
                        @if ($errors->has('Descripcion'))
                            <span class="error text-danger"
                                for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
                        @endif
                    </div>
                </div>
                @endif
                {{-- Boton Guardar --}}

                <div class="container text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <br>
            </form>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
@endsection
