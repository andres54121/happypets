@extends('layouts.Shoppingcart')

@section('title', 'Cambiar Contraseña')
    <!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'profile-page sidebar-collapse')
    <!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
@endsection

@section('content')

    <!--jumbotron-->
    <div class="page-header header-filter" data-parallax="true"
        style="background-image: url({{ asset('img/cover.jpg') }})">
    </div>
    <!-- end jumbotron-->

    <div class="main main-raised">
        <div class="container">
            <br>
            <form action="{{ route('changePassword') }}" method="post" class="form-horizontal">
                @csrf
                <div class="text-center ">
                    <h3 for="name" class="title">Cambio de Contraseña</h3>
                </div>
                <div class="container text-center row">
                    <div class="form-group col-md-4">
                        <label for="SMza"><b>Contraseña Actual</b></label>
                        <input type="password" name="passwordActual" class="form-control" placeholder="Ingrese una contraseña" required>
                        @if ($errors->has('passwordActual'))
                            <span class="error text-danger" for="input-passwordActual">{{ $errors->first('passwordActual') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="SMza"><b>Contraseña Nueva</b></label>
                        <input type="password" name="password" class="form-control" placeholder="Ingrese una nueva contraseña" required>
                        @if ($errors->has('password'))
                            <span class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="SMza"><b>Confirmar Contraseña</b></label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="" required>
                    </div>
                </div>

                {{-- Boton Guardar --}}

                <div class="container text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <br>
            </form>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
@endsection
