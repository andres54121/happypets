{{-- Formulario Para el Edit --}}
@if ( !empty ( $mascota->id) )
    <div class="row">
        <div class="form-group col-md-3">
            <label for="animal">Especie:</label>
            <input type="text" name="animal" class="form-control" placeholder="Ingrese la especie..." autofocus required value="{{ old('animal', $mascota->animal) }}">
            @if ($errors->has('animal'))
                <span class="error text-danger" for="input-animal">{{ $errors->first('animal') }}</span>
            @endif
        </div>

        <div class="form-group col-md-3">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre', $mascota->Nombre) }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-1">
            <label for="edad">Edad:</label>
            <input type="number" name="edad" class="form-control" placeholder="Años de la mascota" required value="{{ old('edad', $mascota->edad) }}">
            @if ($errors->has('edad'))
                <span class="error text-danger" for="input-edad">{{ $errors->first('edad') }}</span>
            @endif
        </div>

        <div class="form-group col-md-2">
            <label>tiempo</label>
            <select class="form-control" name="tiempo">
            <option value="Meses">Meses</option>
            <option value="Años">Años</option>
            </select>
            @if ($errors->has('tiempo'))
                <span class="error text-danger" for="input-tiempo">{{ $errors->first('tiempo') }}</span>
            @endif
        </div>
        <div class="form-group col-md-3">
            <label for="tel">Contacto:</label>
            <input type="tel" name="tel" class="form-control" placeholder="Numero de Telefono" required value="{{ old('tel', $mascota->tel) }}">
            @if ($errors->has('tel'))
                <span class="error text-danger" for="input-tel">{{ $errors->first('tel') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="DescripMedica">Descripcion:</label>
        <textarea rows="6" class="form-control" placeholder="Es de color..." name="DescripMedica" required>{{ old('DescripMedica', $mascota->DescripMedica) }}</textarea>
        @if ($errors->has('DescripMedica'))
            <span class="error text-danger" for="input-DescripMedica">{{ $errors->first('DescripMedica') }}</span>
        @endif
    </div>
@else
{{-- Formulario Para el Create --}}

<div class="row">
        <div class="form-group col-md-3">
            <label for="animal">Especie:</label>
            <input type="text" name="animal" class="form-control" placeholder="Ingrese la especie..." autofocus required value="{{ old('animal') }}">
            @if ($errors->has('animal'))
                <span class="error text-danger" for="input-animal">{{ $errors->first('animal') }}</span>
            @endif
        </div>

        <div class="form-group col-md-3">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre') }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-1">
            <label for="edad">Edad:</label>
            <input type="number" name="edad" class="form-control" placeholder="Años de la mascota" required value="{{ old('edad') }}">
            @if ($errors->has('edad'))
                <span class="error text-danger" for="input-edad">{{ $errors->first('edad') }}</span>
            @endif
        </div>

        <div class="form-group col-md-2">
            <label>tiempo</label>
            <select class="form-control" name="tiempo">
            <option value="Meses">Meses</option>
            <option value="Años">Años</option>
            </select>
            @if ($errors->has('tiempo'))
                <span class="error text-danger" for="input-tiempo">{{ $errors->first('tiempo') }}</span>
            @endif
        </div>

        <div class="form-group col-md-3">
            <label for="tel">Contacto:</label>
            <input type="tel" name="tel" class="form-control" placeholder="Numero de Telefono" required value="{{ old('tel') }}">
            @if ($errors->has('tel'))
                <span class="error text-danger" for="input-tel">{{ $errors->first('tel') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="DescripMedica">Descripcion:</label>
        <textarea rows="6" class="form-control" placeholder="Es de color..." name="DescripMedica" required>{{ old('DescripMedica')}}</textarea>
        @if ($errors->has('DescripMedica'))
            <span class="error text-danger" for="input-DescripMedica">{{ $errors->first('DescripMedica') }}</span>
        @endif
    </div>
@endif

<input name="img[]" type="file" id="file" multiple="multiple">
@if ($errors->has('img.*'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img.*') }}</span>
@endif
@if ($errors->has('img'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img') }}</span>
@endif


<div id="container-input">
    <div class="wrap-file">
        <div id="preview-images"></div>
    </div>
</div>
