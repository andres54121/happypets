@extends('layouts.main', ['activePage' => 'plantas', 'titlePage' => 'Listado de Mascotas'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Mascotas</h4>
                                <p class="card-category">Mascotas que estan en adopcion</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{route("mascotas.create")}}" class="btn btn-sm btn-success">Agregar nueva mascota</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Especie</th>
                                            <th>Nombre</th>
                                            <th>Edad</th>
                                            <th>Tel</th>
                                            <th>Descripcion</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($mascotas as $mascota)
                                            <tr>
                                                <td>{{$mascota->animal}}</td>
                                                <td>{{$mascota->Nombre}}</td>
                                                <td>{{$mascota->edad}} {{$mascota->tiempo}}</td>
                                                <td>{{$mascota->tel}}</td>
                                                <td>{{$mascota->DescripMedica}}</td>
                                                <td class="td-actions text-right">
                                                    <a href="{{route("mascotas.edit", $mascota->id)}}" class="btn btn-warning" title="Editar" rel="tooltip">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <form action="{{route("mascotas.destroy", $mascota->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de eliminar esta mascota?')">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger" type="submit" title="Eliminar" rel="tooltip">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{ $mascotas->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
