@extends('layouts.main', ['activePage' => 'roles', 'titlePage' => 'Editar rol'])
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('roles.update', $rol->id) }}" method="post" class="form-horizontal">

                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Rol</h4>
                            <p class="card-category">Editar Rol</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" class="form-control" value="{{ old('name', $rol->name)}}" autofocus required>
                                    @if ($errors->has('name'))
                                        <span class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="description" class="col-sm-2 col-form-label">Descripcion:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="description" class="form-control" value="{{ old('description', $rol->description)}}" required>
                                    @if ($errors->has('description'))
                                        <span class="error text-danger" for="input-description">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Listado de Permisos --}}
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-tittle">Lista de Permisos</h4>
                            <p class="card-category">Seleccione los permisos que tendra este Rol</p>
                        </div>

                        <div class="card-body">
                            <div class="row input-group">
                                @foreach ($permissions as $permission)
                                    <div class="col-md-4">
                                        {!! Form::checkbox(
                                            "permission[$permission->id]",
                                            $permission->id,
                                            // count($rol->permissions->where('id', $permission->id)),
                                            $rol->hasPermissionTo($permission->id)
                                        ) !!}
                                        <span>{{ $permission->description }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
