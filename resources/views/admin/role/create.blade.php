@extends('layouts.main', ['activePage' => 'roles', 'titlePage' => 'Nuevo rol'])
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route("roles.store")}}" method="post" class="form-horizontal">
                    @csrf
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Agregar nuevo Rol</h4>
                            <p class="card-category">Ingresar Datos</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" class="form-control" placeholder="Ingrese el nombre del rol..." autofocus required value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="description" class="col-sm-2 col-form-label">Descripcion:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="description" class="form-control" placeholder="Ingrese una descripcion..." required value="{{ old('description') }}">
                                    @if ($errors->has('description'))
                                        <span class="error text-danger" for="input-description">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Listado de Permisos --}}
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-tittle">Lista de Permisos</h4>
                            <p class="card-category">Seleccione los permisos que tendra este Rol</p>
                        </div>

                        <div class="card-body">
                            <div class="row input-group">
                                @foreach ($permissions as $permission)
                                    <div class="col-md-4">
                                        {!! Form::checkbox(
                                            "permission[$permission->id]",
                                            $permission->id,
                                        ) !!}
                                        <span>{{ $permission->description }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
