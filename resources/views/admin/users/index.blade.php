@extends('layouts.main', ['activePage' => 'users', 'titlePage' => 'Lista de Usuarios'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Usuarios</h4>
                                <p class="card-category">Usuarios Registrados</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="row">
                                    @if(@Auth::user()->hasRole('SuperAdmin'))
                                        <div class="col-12 text-right">
                                            <a href="{{route("usuario.create")}}" class="btn btn-sm btn-facebook">Nuevo Usuario</a>
                                        </div>
                                    @endif
                                </div>
                                <div class="table-responsive">

                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Fecha de Creacion</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $user)
                                            <tr>
                                                <td>{{$user->name}} {{$user->apellidoPa}} {{$user->apellidoMa}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->created_at}}</td>
                                                <td class="td-actions text-right">
                                                    @if(@Auth::user()->hasRole('SuperAdmin'))
                                                        <a href="{{route("usuario.edit", $user->id)}}" class="btn btn-warning" title="Editar" rel="tooltip">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                        <form action="{{route("usuario.destroy", $user->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de eliminar al usuario?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger" type="submit" title="Eliminar" rel="tooltip">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
