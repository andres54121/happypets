@extends('layouts.main', ['activePage' => 'users', 'titlePage' => 'Nuevo Usuario'])
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route("usuario.store")}}" method="post" class="form-horizontal">
                    @csrf
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Usuario</h4>
                            <p class="card-category">Ingresar Datos</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="apellidoPa" class="col-sm-2 col-form-label">Apellido Paterno:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="apellidoPa" class="form-control" placeholder="Ingrese el apellido paterno..." required value="{{ old('apellidoPa') }}">
                                    @if ($errors->has('apellidoPa'))
                                        <span class="error text-danger" for="input-apellidoPa">{{ $errors->first('apellidoPa') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="apellidoMa" class="col-sm-2 col-form-label">Apellido Materno:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="apellidoMa" class="form-control" placeholder="Ingrese el apellido materno" required value="{{ old('apellidoMa') }}">
                                    @if ($errors->has('apellidoMa'))
                                        <span class="error text-danger" for="input-apellidoMa">{{ $errors->first('apellidoMa') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="email" class="col-sm-2 col-form-label">Correo:</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" placeholder="Ingrese el correo electronico" required value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="password" class="col-sm-2 col-form-label">Contraseña:</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control" placeholder="Ingrese una contraseña" required>
                                    @if ($errors->has('password'))
                                        <span class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="confirm-password" class="col-sm-2 col-form-label">Confirmar contraseña:</label>
                                <div class="col-sm-8">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="" >
                                </div>
                            </div>
                            <input  style="display: none" type="text" name="rol" class="form-control" value="Admin">
                        </div>
                    </div>
                    {{-- Listado de Roles --}}
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-tittle">Lista de Roles</h4>
                            <p class="card-category">Seleccione el rol que tendra este usuario</p>
                        </div>

                        <div class="card-body">
                            <div class="row input-group">
                                @foreach ($roles as $role)
                                    @if ($role->description != 'Cliente')
                                        <div class="radio col-md-4">
                                            {!! Form::radio(
                                                "role",
                                                $role->id
                                            ) !!}
                                            <span>{{ $role->name }}</span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
