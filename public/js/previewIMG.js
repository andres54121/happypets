(function () {

    const $seleccionArchivos = document.querySelector("#file");

    // Escuchar cuando cambie
    $seleccionArchivos.addEventListener("change", () => {
        // Quitar imagenes
        clearPreviews();

        // Los archivos seleccionados, pueden ser muchos o uno
        const archivos = $seleccionArchivos.files;

        for ( var i = 0; i < archivos.length; i++ ) {
			var thumbnail_id = Math.floor( Math.random() * 30000 ) + '_' + Date.now();
            // Ahora tomamos el primer archivo, el cual vamos a previsualizar
            const Imagenes = archivos[i];
			createPreview(Imagenes, thumbnail_id);
		}
    });

    var createPreview = function (Imagenes, thumbnail_id) {
        var thumbnail = document.createElement('div');
		thumbnail.classList.add('thumbnail', thumbnail_id);
		thumbnail.dataset.id = thumbnail_id;

		thumbnail.setAttribute('style', `background-image: url(${ URL.createObjectURL( Imagenes ) })`);
        document.getElementById('preview-images').appendChild(thumbnail);
    }

    var clearPreviews = function () {
		document.querySelectorAll('.thumbnail').forEach(function (thumbnail) {
			thumbnail.remove();
		});
	}
})();
