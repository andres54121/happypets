<?php

namespace Database\Seeders;

use App\Http\Controllers\admin\UserController;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Usuarios
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['create'] ],
        //     [ 'description' => 'Creacion de usuarios' ],
        //     // [ 'guard_name' => '' ],
        //     // [ '' => '' ],
        // );
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['edit'] ],
        //     [ 'description' => 'Editar usuarios' ],
        // );
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['destroy'] ],
        //     [ 'description' => 'Eliminar usuarios' ],
        // );

        // Plantas
        Permission::updateOrCreate(
            [ 'name' => 'mascota.create' ],
            [ 'description' => 'Agregar una mascota al catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'mascota.edit' ],
            [ 'description' => 'Editar una mascota del catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'mascota.destroy' ],
            [ 'description' => 'Eliminar una mascota del catalogo.' ],
        );
        // Permission::updateOrCreate(
        //     [ 'name' => '' ],
        //     [ 'description' => '' ],
        // );
    }
}
