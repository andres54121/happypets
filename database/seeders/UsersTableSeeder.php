<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super',
            'apellidoPa' => 'Admin',
            'apellidoMa' => 'XD',
            'email' => 'admin@admin.com',
            'rol' => 'Admin',
            'password' => Hash::make('password'),
        ])->assignRole('SuperAdmin');
    }
}
