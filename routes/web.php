<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::middleware(['auth'])->prefix('administracion')->group( function(){
    Route::resource("usuario", "admin\UserController");
    Route::resource("permisos", "admin\PermissionController")->only('index');
    Route::resource("roles", "admin\RoleController");

    Route::resource("mascotas", "MascotaController");
    Route::get('mascotas/eliminarimagen/{id}', 'MascotaController@eliminarimagen')->name('mascotas/eliminarimagen');
});


